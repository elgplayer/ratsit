import os
import json
import logging
import pickle
import threading
import queue
import time
import datetime


import requests
from bs4 import BeautifulSoup


from src import utils
from src import fetcher  
from src import scraper


# Get the current datetime
time_now = str(datetime.datetime.now())

# Create logger
logger = utils.create_logger()
# Logger init
logger = logging.getLogger(__name__)
# Start the logging
logger.info('Started! | {}'.format(time_now))

scraper.create_people_db()