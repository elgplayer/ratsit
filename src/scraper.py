import os
import json
import logging
import pickle
import threading
import queue
import time
import datetime


import requests
from bs4 import BeautifulSoup


from src import utils
from src import fetcher  


# Logger init
logger = logging.getLogger(__name__)

queue = queue.Queue()

def create_people_db():

    # Error list
    big_error_list = []

    # Init the folder structure
    utils.init_folder()

    # Loads the kommun dict
    kommun_dict = utils.load_kommun_list()

    # Get all the kommun names and paths
    kommun_list_data = kommun_dict['kommun_list']
    kommun_folder = kommun_dict['kommun_folder']
    kommun_list_file_path = kommun_dict['list_file_path']

    # For some reason; create a copy of the kommun list
    data = kommun_list_data[:]

    # Go through the data
    for i in range(len(data)):

        # Check that the item is not already pickled
        #if kommun_list_data[i]['pickled'] == False
        # Dorotea Kommun for easy debug
        if i == 29:
        
            # Kommun
            data_1 = data[i]
            data_2 = fetcher.get_layer(data_1, False)
                    
            logging.info("### Starting Kommun: {} ({} people) | Number of Threads: {} ###".format(kommun_list_data[i]['name'], kommun_list_data[i]['number'], len(data_2)))
                    
            try:
                
                # Ge the start time
                start = time.time()
        
                # Create the threads
                threads = [threading.Thread(target=fetcher.fetch_post_nr, args=(data_2[k], k, queue)) for k,x in enumerate(data_2)]
                
                # Go through all the threads and start the processes
                for thread in threads:
                    thread.start()

                # Go through the threads after they are started
                for thread in threads:

                    # Join the threads
                    thread.join()
                    
                    # Get the return value from the thread
                    tmp_data = queue.get()

                    # Add the data to the dictionary
                    data_2[tmp_data['index']]['data'] = tmp_data['data']
                
                # Clear the queue for more munipalities
                with queue.mutex:
                    queue.queue.clear()

                # Add the data to the kommun dict
                data[i]['data'] = data_2
                
                # Set the pickle path
                pickle_path = "{}/{}.pickle".format(kommun_folder, kommun_list_data[i]['name'])
                pickle.dump(data[i], open(pickle_path, "wb"))
            
                # Save the kommun list
                kommun_list_data[i]['pickled'] = True
                pickle.dump(kommun_list_data, open(kommun_list_file_path, "wb"))
                logging.info("Kommun list: {} saved! | Elapsed Time: {}".format(kommun_list_data[i]['name'], (time.time() - start)))
        
            except Exception as e:
            
                logging.warning("Kommun: {} | ERROR: {}".format(kommun_list_data[i]['name'], e))
                big_error_list.append(e)

