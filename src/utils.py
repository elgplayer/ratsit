import os
import json
import pickle
import logging
import datetime
import sys


from src import fetcher


# Logger init
logger = logging.getLogger(__name__)


def create_logger():
    '''
    Creates a logger to log the script
    
    return: Logger object
    '''
    
    #init_structure()
    root_logger = None
    
    date_today = datetime.datetime.today().strftime('%Y_%m_%d__%H_%M_%S')
    log_file_name = "{}.log".format(date_today)
    log_path = "log"

    log_file_path = "{}/{}".format(log_path, log_file_name)

    # Create folder if not exists
    if not os.path.exists(log_path):
        print("Creating logging directory: {}".format(log_path))
        os.makedirs(log_path, exist_ok=True)

    # Remove old logs
    remove_old_logs(log_path, 5)

    # Create logger
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.DEBUG)
    
    if root_logger.handlers:
        root_logger.handlers.pop()
        print("Removing old logger from memory...")
        
    # Create file handler and set level to debug
    fh = logging.FileHandler(log_file_path, mode = 'w')
    fh.setLevel(logging.DEBUG)
    
    # Create console handler with a higher log level
    ch = logging.StreamHandler(stream=sys.stdout)
    ch.setLevel(logging.INFO)
    
    # Create formatter and add it to the handlers
    formatter = logging.Formatter('[%(asctime)s] %(levelname)8s --- %(message)s ' +
                                  '(%(filename)s:%(lineno)s)', 
                                  datefmt='%Y-%m-%d %H:%M:%S')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
        
    # Add the handlers to the logger
    root_logger.addHandler(fh)
    root_logger.addHandler(ch)
    

    return logger


def remove_old_logs(log_path, max_nr_of_logs=5):
    '''
    Removes logs files if the amount of log file execedes a criteria

    Attributes:
        * log_path (str): Where the log are located
        * max_nr_of_logs (int) [default=5]: Max number of logs that are allowed to be present 
    '''

    # Check for that the max amount of logs is not negative
    if max_nr_of_logs < 0:
        return

    files = os.listdir(log_path) # Get the files in the log folder
    file_time_list = [] # Init empty list

    # The max_nr_of_logs is already True
    if len(files) < max_nr_of_logs:
        return

    # Go thorugh the log files and get their ctime (system time)
    for k,x in enumerate(files):
        file_time = os.path.getctime("{}/{}".format(log_path, files[k]))
        file_time_list.append(file_time)

    # Sort the list based on the file_times
    file_time_list = [x for _,x in sorted(zip(file_time_list,files))]

    # Slice the list to get all the files that needs to be deleted
    files_to_delete = file_time_list[:-max_nr_of_logs]

    # Go through the list
    for file in files_to_delete:

        # Log file path
        log_to_delete = "{}/{}".format(log_path, file)

        # Delete the file
        os.remove(log_to_delete)


def init_folder():
    '''
    Inits the base structure of the project
    '''

    folders = ['log', 'deb', 'output', 'output/kommuner_responses', 
               'output/pickle', 'output/pickle/kommuner', 'output/errors']

    # Go through the folders
    for folder in folders:

        actual_path = "{}".format(folder)

        # Check if the path exists, else create the directory
        if not os.path.exists(actual_path):

            # Creates directoreies recursivly 
            logging.info("Creating folder: {}".format(actual_path))
            os.makedirs(actual_path, exist_ok=True)


def check_exists(file_path):
    '''
    Checks if a file exists

    Return: 0 if not exists, else 1
    ''' 
    if not os.path.exists(file_path):

        return 0

    return 1


def write_json(data, file_path, logger_msg="Data written to disk"):
    '''
    Writes JSON data to disk

    Attributes:
        * data (ANY): Data that will be written to file
        * file_path (str): Path where to store the data
        * logger_msg (str): Logger debug message
    '''

    # Open the file
    with open(file_path, 'w', encoding='utf-8') as outfile:
        
        # Writes the file as JSON
        json.dump(data, outfile, indent=4, 
                  ensure_ascii=False)


def load_pickle(file_path):

    return pickle.load(open(file_path, "rb"))


def write_pickle(file_path, data):

    full_file_path = file_path + ".pickle"

    pickle.dump(data, open(full_file_path, "wb"))

    
def load_kommun_list(overwrite=False):

    # Kommun dictionary!
    kommun_list = "kommun_list.pickle"
    folder_path =  "output/pickle"
    kommun_folder = "{}/kommuner".format(folder_path)
    kommun_list_file_path = "{}/{}".format(folder_path, kommun_list)

    log_str = "Kommun_list name: {} | Kommun_folder: {} | Kommun_list_file_path: {}".format(kommun_list, kommun_folder, kommun_list_file_path)
    logging.info(log_str)

    if check_exists(kommun_list_file_path) == 0 or overwrite == True:

        # Check if the path exists, else create the directory
        if not os.path.exists(folder_path):

            # Creates directoreies recursivly 
            os.makedirs(folder_path, exist_ok=True)

        # Download the list
        person_url = "personer"
        kommun_list_data = fetcher.get_layer(person_url)

        # Set the pickled status to False
        for k,x in enumerate(kommun_list_data):

            kommun_list_data[k]['pickled'] = False

    else:
        
        # Opens the kommun list from file
        kommun_list_data = pickle.load(open(kommun_list_file_path, "rb"))

    return_dict = {'kommun_list': kommun_list_data, 
                   'kommun_folder': kommun_folder, 
                   'list_file_path': kommun_list_file_path}

    return return_dict
