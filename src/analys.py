# -*- coding: utf-8 -*-
"""
Created on Fri Jan  3 10:36:46 2020

@author: Carl
"""


import json
import os
import pickle
import logging


from src import utils

# Logger init
logger = logging.getLogger(__name__)

kommun_list = "kommun_list.pickle"
folder_path =  "../output/pickle"
kommun_folder = "{}/kommuner".format(folder_path)
kommuner = os.listdir(kommun_folder)
#kommun_list_file_path = "{}/{}".format(folder_path, kommun_list)

people_list = []
empty_people = []
error_list = []
kommun_list_utils = utils.load_kommun_list()

kommun_list = kommun_list_utils['kommun_list']

#%%

counter = 0

# Loads the People list-
for ix, iy in enumerate(kommuner):
    
    # Parses the kommun pickle file path
    selected_kommun_path = "{}/{}".format(kommun_folder, iy)
    #selected_kommun_path = "{}/{}.pickle".format(kommun_folder, kommun_list[ix]['name'])
    
    #try:

    # Loads the kommuner from a pickle file
    selected_kommun_data = pickle.load(open(selected_kommun_path, "rb"))


    # Iterate over the data
    sel_data_1 = selected_kommun_data['data']
    #kommun_list[ix]['data'] = sel_data_1
    
    for k,x in enumerate(sel_data_1):
        
        sel_data_2 = sel_data_1[k]['data']
        
        for kx,ky in enumerate(sel_data_2):
            
            if sel_data_2[kx] != "PARSING_ERROR" and sel_data_2[kx] != "EMPTY":
            
                sel_data_3 = sel_data_2[kx]['data']
                
                # Check that the data is not empty
                if sel_data_3 != {}:
    
                    # Check if address!
                    if "age" in sel_data_3[0] or "dold_traff" in sel_data_3[0]:
                        
                        for kx_3, ky_3 in enumerate(sel_data_3):
                            
                            people_list.append({'layer': sel_data_2[kx]['layer'], 'url': sel_data_2[kx]['url'], 'people': sel_data_3[kx_3]})
                    
                    else:
                        
                        for kx_2, ky_2 in enumerate(sel_data_3):
                            
                            if sel_data_3[kx_2] != "PARSING_ERROR" and sel_data_3[kx_2] != "EMPTY":
                            
                                sel_data_4 = sel_data_3[kx_2]['data']
                                
                                if len(sel_data_4) > 0:
                                
                                    # Check if address!
                                    if "age" in sel_data_4[0] or "dold_traff" in sel_data_4[0]:
                                        
                                        for kx_3, ky_3 in enumerate(sel_data_4):
                                            
                                            if 'data' in sel_data_4[kx_3]:
                                            
                                                if sel_data_4[kx_3]['data'] == {}:
                                                    
                                                    error_list.append(sel_data_4[kx_3])
                                                    
                                            else:
                                                
                                                people_list.append({'layer': sel_data_3[kx_2]['layer'], 'people': sel_data_4[kx_3], 'url': sel_data_3[kx_2]['url']})
                                     
                                else:
                                    
                                    error_list.append(sel_data_4)
                                    
                            else:
                                
                                error_list.append(sel_data_3)
                                
            else:
                
                error_list.append(sel_data_2)
                
                                
        counter += 1
    

#%%

# Gifta par under 18
for k,x in enumerate(people_list):
    if "dold_traff" not in people_list[k]['people']:
        if people_list[k]['people']['married'] == True and people_list[k]['people']['age'] < 18:
            print(x)            

#%%
   
# Create dict with all people where the key is their address         
address = {}
for k,x in enumerate(people_list):

    layer_str = '_'.join(people_list[k]['layer'])
    
    if "dold_traff" not in people_list[k]['people']:
        
        addres_str = people_list[k]['people']['address']
        addres_str = addres_str.replace(" ", "_")
        
        dict_key = layer_str + "_" + addres_str
    
    else:
    
        dict_key = layer_str
        
        
    try:
    
        address[dict_key]['people'].append(people_list[k]['people'])
                
    except:
    
        address[dict_key] = {'people': [people_list[k]['people']], 'url': people_list[k]['url']}

#%%
i = 0
total_people = 0

flashback_str = ""
print_list = []
number_list = []
csv_list = []

header = "Antal (På addresen),Män,Kvinnor,Genomsnittsålder,Address,URL\n"

# Goes through the address
for k,x in enumerate(address):

    if len(address[x]['people']) >= 7:# and "lgh" in x:
        
        
        age = 0
        gender_dict = {'Man': 0, 'Kvinna': 0}
        
        for kx, ky in enumerate(address[x]['people']):
            
            age += address[x]['people'][kx]['age']
            gender_dict[address[x]['people'][kx]['gender']] += 1
        
        
        address_str = address[x]['people'][0]['address']
        
        number_of_people = len(address[x]['people'])
        age = age / number_of_people
        age = round(age, 1) # Round to 1 decimal
        
        man_to_women = gender_dict['Man'] / number_of_people
        
        if age < 60:# and man_to_women < 0.2:
            
            total_people += number_of_people
    
            ratsit_url = "http://ratsit.se" + address[x]['url']
            man_str = "Män: {}".format(gender_dict['Man'])
            kvinna_str = "Kvinnor: {}".format(gender_dict['Kvinna'])
        
            csv_str = "{},{},{},{},{},{}\n".format(number_of_people, gender_dict['Man'], gender_dict['Kvinna'], age, address_str,ratsit_url)
            print_str = "Antal: {} ({} & {}) | Genomsnitts ålder: {} | Address: {} | URL: {}".format(number_of_people, man_str, kvinna_str, age, address_str, ratsit_url)
            
            
            print_list.append(print_str)
            number_list.append(number_of_people)
            csv_list.append(csv_str)
            i += 1

sorted_list = [x for _,x in reversed(sorted(zip(number_list, print_list)))]
csv_list = [x for _,x in reversed(sorted(zip(number_list, csv_list)))]

for line in sorted_list:
    flashback_str += line + "\n"
    
with open("../output/flashback/suspect_address.csv", "w", encoding="utf-8") as f:
    
    f.write(header)
    
    for line in csv_list:
        
        f.write(line)
        
    
print("TOTAL number of address: {} | Total number of people: {}".format(i, total_people))
            
            
            
            
            
            
            




