import pickle
import os
import logging


import requests 
from bs4 import BeautifulSoup


from src import utils

# Logger init
logger = logging.getLogger(__name__)


def normal_parse(data, layers, category):

    export_list = []
    error_path = "output/errors/{}".format('_'.join(layers))

    # Lets parse the data
    soup = BeautifulSoup(data, 'lxml')

    find_2 = None
    try:
        if category == "Alla Kommuner":
    
            find_1 = soup.find('div', class_='x-left col-md-12 col-lg-12 x-left--org-padding x-left--no-x-m-small-screens ')
            find_2 = find_1.find_all('div', class_='link-structure__col0 col-xs-12 col-md-12')   
  
        if category == "Gator (antal personer)":
    
            find_1 = soup.find_all('div', class_='content-block')
            find_2 = find_1[1].find_all('div', class_='link-structure__col2 col-xs-12 col-md-12')
    
        if category == "Gatuadress (antal personer)":
    
            try:
                find_1 = soup.find_all('div', class_='content-block')
                find_2 = find_1[1].find_all('div', class_='link-structure__col3 col-xs-12 col-md-12')
            except:
                logging.debug("Parsing_Error | Layer: {}".format(layers))
                return_var = ["PARSING_ERROR"]
                return return_var
        
    except Exception as e:
        #utils.write_json(soup.text, error_path)
        utils.write_pickle(error_path, data)
        logging.warning("ERROR: {} | Category: {} | Layers: {}".format(e, category, layers))
        raise "ERROR!"
    
    # Check that find_2 is not empty
    if find_2 == None:

        raise "ERROR!"

    # Gets the diffrent "groupings"
    for k,x in enumerate(find_2):
        
        # Find all with links
        matches = find_2[k].find_all('a', href=True) 
        number_of_matches = find_2[k].find_all('span') # Number of matches
        
        # Goes through each group
        for kx, ky in enumerate(matches):      
            
            matches_namn = matches[kx].text
            matches_url = matches[kx].attrs['href']

            matches_number = number_of_matches[kx].text
            matches_number = matches_number.replace('\xa0', '')
            matches_number = matches_number.replace('(', '')
            matches_number = matches_number.replace(')', '')
            
            # Hmm
            if category == "Alla Kommuner":

                layer = matches_namn
                tmp_layer = [layer]
           
            else: 

                layer = matches_namn
                tmp_layer = layers[:]
                tmp_layer.append(layer)   

            export_dict = {'name': matches_namn, 'url': matches_url, 'number': matches_number, 'data': {}, 'layer':  tmp_layer}
            
            # Append the export dict
            export_list.append(export_dict)

    return export_list


def parse_postort(data, layers):
    '''
    Parses all the diffrent post_nr links

    Attributes:
        * data (str): Raw text data downloaded to be parsed (HTML)
        * layers (list): List which show where the data should be "placed"
    '''

    postort_list = []

    # Lets parse the data
    soup = BeautifulSoup(data, 'lxml')  
    find_1 = soup.find_all('div', class_='content-block')
    find_2 = find_1[1].find_all('div', class_='link-structure__col1 col-xs-12 col-md-12')

    # Goes through the groupings
    for k,x in enumerate(find_2):
        
        matches = find_2[k].find_all('a', href=True)
        number_of_matches = find_2[k].find_all('span')
        
        # Goes through the items in the grouping
        for kx, ky in enumerate(matches):      
            
            matches_text = matches[kx].text
            matches_namn = matches_text.split(' - ')[0]
            matches_namn = matches_namn.replace("/", "")
            matches_post_nr = matches_text.split(' - ')[1]
            matches_url = matches[kx].attrs['href']

            matches_number = number_of_matches[kx].text
            matches_number = matches_number.replace('\xa0', '')
            matches_number = matches_number.replace('(', '')
            matches_number = matches_number.replace(')', '')
            
            # Layers.
            current_layer = matches_namn
            tmp_layer = layers[:]
            tmp_layer.append(current_layer)
            
            export_dict = {'name': matches_namn, 'url': matches_url, 
                            'number': matches_number, 'post_nr': matches_post_nr, 
                            'data': {}, 'layer':  tmp_layer}

            postort_list.append(export_dict)


    return postort_list   
            

def check_for_people(data):
    '''
    Check if people are present in the data

    Attributes:
        * data (str): Raw text data downloaded to be parsed (HTML)
    
    return: Boolean value (int)
    '''

    is_people = 0

    if 'search-list-item' in data:

        is_people = 1

    return is_people


def people_parse(data):

    export_list = []
    gift_dict = {"Ej gift": False, "Gift": True}

    # Lets parse the data
    soup = BeautifulSoup(data, 'lxml')  
    find_1 = soup.find('div', class_='x-left col-md-12 col-lg-12 x-left--org-padding x-left--no-x-m-small-screens ')
    find_2 = find_1.find_all('div', class_='search-list-item')
    
    for k,x in enumerate(find_2):
        
        match = find_2[k]
        smaller_match = match.find_all('a', href=True)[0]
        ratsit_url = smaller_match.attrs['href'] # Get the ratsit URL
        
        if "Dold träff"  not in match.text:

            # Some mild parsing
            name_and_age = smaller_match.find('span', {'class': 'name'}).text
            name_and_age = name_and_age.replace("\n", "")
            if name_and_age[-1] == " ":
                name_and_age = name_and_age[:-1]
            
            name = name_and_age.split(", ")[0]
            age = int(name_and_age.split(", ")[1])
            
            address = smaller_match.find('span', {'class': 'address'}).text
            hidden_address_city = smaller_match.find('span', {'class': 'search-list-name-address__city hidden-md-up'}).text
            address = address[:-len(hidden_address_city)] # Remove the hidden address
            if address[-1] == " ":
                address = address[:-1]
            
            gender = match.find('i', {'class':'sr-only'}).text
            married = match.find_all('i', {'class':'sr-only'})[1].text
            married = gift_dict[married]
            
            bolags_engement = match.find('li', {'title': "Har bolagsengagemang"})
            if bolags_engement != None:
                bolags_engement = bolags_engement.text
                bolags_engement = bolags_engement.replace("\n", "")
                bolags_engement = int(bolags_engement)
            else:
                bolags_engement = 0
                
        
            export_dict = {"name": name, "age": age, 
                           "gender": gender, "address": address, 
                           "hidden_address_city": hidden_address_city,
                           "married": married, "bolags_engement": bolags_engement,
                           "ratsit_url": ratsit_url}
        else:
            
            export_dict = {"ratsit_url": ratsit_url, "dold_traff": 1}
        
        export_list.append(export_dict)


    return export_list