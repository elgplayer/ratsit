# -*- coding: utf-8 -*-
"""
Created on Tue Dec 31 16:13:12 2019

@author: Carl
"""

import pickle
import os
import logging


import requests 
from bs4 import BeautifulSoup


from src import utils
from src import data_parser

# Logger init
logger = logging.getLogger(__name__)


def fetch_layer(url, layers, post_nr, overwrite=False):

    base_url = 'https://www.ratsit.se'
    full_url = base_url + url
    #print(full_url)

    if layers == "top_data":
        name = "kommuner_top_data"
        folder_path = ""
    else:
        name = '_'.join(layers)
        folder_path = '/'.join(layers)
        name = layers[-1]
    
    # Create layer_path
    layer_path = "../output/kommuner_responses/{}".format(folder_path)
    layer_path = layer_path.replace(":", "_")

    # Check if the path exists, else create the directory
    if not os.path.exists(layer_path):

        # Creates directoreies recursivly 
        os.makedirs(layer_path, exist_ok=True)

    # File path
    name = name.replace(":", '_')
    name = name.replace("/", "")
    
    
    file_path = "{}/{}.pickle".format(layer_path, name)

    # Check if the file exists 
    if utils.check_exists(file_path) == 0 or overwrite == True:
 
        # We need to specifiy the headers so the site is rendered equally
        headers = {
            'User-Agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36",
            'Accept': "*/*",
            'Cache-Control': "no-cache",
            'Host': "www.ratsit.se",
            'Accept-Encoding': "gzip, deflate",
            'Connection': "keep-alive",
            'cache-control': "no-cache"
            }

        # Download the data
        data = requests.request("GET", full_url, headers=headers).text

        # Save the data to disk
        pickle.dump(data, open(file_path, 'wb'))

    else:

        # if the file already 
        data = pickle.load(open(file_path, 'rb'))


    return data


def get_layer(data_dict, overwrite=False):

    area_categories = ["Alla Kommuner", "Postort", "Gator (antal personer)", "Gatuadress (antal personer)"]
    export_list = []

    post_nr = None

    if data_dict == "personer":

        url = "/personer"
        layers = "top_data"

    else:

        try:

            url = data_dict['url']
            layers = data_dict['layer']

        except:

            logging.debug("Parsing error!")
            return ["PARSING_ERROR"]
            
        if 'post_nr' in data_dict:
            
            post_nr = data_dict['post_nr']
            post_nr = post_nr.replace(" ", "_")
            post_nr_name = data_dict['name'] + "_" + post_nr
            
            layers.append(post_nr_name)

    # Fetch the data
    while_i = 0
    data = fetch_layer(url, layers, post_nr, overwrite)
    while "The service is unavailable." in data:
        data = fetch_layer(url, layers, post_nr, overwrite)
        while_i += 1
        print("Trying again...")
        
        if while_i == 10:
            print("Breaking while I! URL: {}".format(url))
            break
            
    # Check if people are in the data
    is_people = data_parser.check_for_people(data)

    # Check for people
    if is_people == 0:

        for category in area_categories:
            if category in data:
                #print("Current category: ", category)
                break

        if category == "Alla Kommuner":

            export_list = data_parser.normal_parse(data, layers, category)

        if category == "Postort":
            
            export_list = data_parser.parse_postort(data, layers)

        if category == "Gator (antal personer)":
            
            export_list = data_parser.normal_parse(data, layers, category)

        if category == "Gatuadress (antal personer)":
            
            export_list = data_parser.normal_parse(data, layers, category)

    else:

        export_list = data_parser.people_parse(data)

    return export_list


def fetch_post_nr(selected_data_2, k, queue):
    '''
    Fetches all the URL for a given Post Nr

    Attributes:
        * selected_data_2: Post_Nr data (dict)
        * k: Index of the Post_nr dict in the kommun list
        * queue: Queue that handles return value for the threading

    '''

    data_3 = get_layer(selected_data_2, False)
    
    print_str = "Thread: {} | Område: {} | Post_nr: {} | Number: {}".format(k, selected_data_2['name'], selected_data_2['post_nr'], selected_data_2['number'])
    print("    ", print_str) # Indent to make it clearer to disingtuish
    logging.debug(print_str)
    
    for kx, ky in enumerate(data_3):
        
        selected_data_3 = data_3[kx]
        data_4 = get_layer(selected_data_3, False)
        
        for kx_2, ky_2 in enumerate(data_4):
            
            try:

                if "age" not in data_4[kx_2]:
                
                    selected_data_4 = data_4[kx_2]
                    data_5 = get_layer(selected_data_4, False)
                    data_4[kx_2]['data'] = data_5
            
            except:
                try:
                    logging.debug("This site is empty!")
                    data_4[kx_2]['data'] = "EMPTY"
                except:
                    data_4 = ["EMPTY"]
                    pass
        
        try:
            data_3[kx]['data'] = data_4
        except:
            pass

    # Return value
    queue.put(({'data': data_3, 'index': k}))