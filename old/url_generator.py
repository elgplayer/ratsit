# -*- coding: utf-8 -*-
"""
Created on Sun May  6 11:18:12 2018

@author: carlelg
"""


import string
import pickle
from concurrent.futures import ThreadPoolExecutor
import concurrent.futures

import requests
from bs4 import BeautifulSoup
import pandas as pd



company_database = []
delayed_results = []
output2 = {}
output3 = {}

base_url = 'https://www.ratsit.se'
top = requests.get(base_url + '/foretag').text

#%%

### KOMMUNER ###

soup = BeautifulSoup(top, 'html.parser')  
block = soup.find('div', class_='x-left col-md-12 col-lg-12 x-left--org-padding x-left--no-x-m-small-screens ')
block2 = block.find_all('div', class_='link-structure__col0 col-xs-12 col-md-12')
#
## DYNAMIC VARIBLE GENERATOR
#
#header = soup.find('p', class_='m-b-0 font-italic')
#header = header.find('strong').text
#header = header.replace('(antal företag)', '')
#header = header.split(' - ')
#
#header_dic = {}
#for k,x in enumerate(header):
#    header_dic[x] = header[k]
#
#
#
## FINDS NUMBER OF COMPANYS FOR EVERY KOMMUN
#
#for k,x in enumerate(header_dic):
#    header_dic[x] = []
#    
    
    
kommun = []
urls = []
antal_kommun = []

for k,x in enumerate(block2):
    abc = block2[k].find_all('a', href=True)
    abcd = block2[k].find_all('span')
    
    for kx, ky in enumerate(abc):      
        tmp = abcd[kx].text
        tmp = tmp.replace('\xa0', '')
        tmp = tmp.replace('(', '')
        tmp = tmp.replace(')', '')
        
        
        kommun.append(abc[kx].text)
        urls.append(base_url + abc[kx].attrs['href'])
        antal_kommun.append(tmp)
    
top_dic = {}


for k,x in enumerate(kommun):
    top_dic[k] = {}
    top_dic[k]['url'] = urls[k]
    top_dic[k]['antal'] = antal_kommun[k]
    top_dic[k]['namn'] = kommun[k]
    
URLS = {}    
for k,x in enumerate(top_dic):
    URLS[k] = top_dic[k]['url']
#%%
    
    
#for ki, xi in enumerate(top_dic):
    
#def foretag_scraper(value):
value = 0
company_database = []
ki = value

### POST ORTER ###
kommun_sel = top_dic[ki]
kommun_top = requests.get(kommun_sel['url']).text

soup = BeautifulSoup(kommun_top, 'html.parser')  

block = soup.find_all('div', class_='content-block')
block2 = block[1].find_all('div', class_='link-structure__col1 col-xs-12 col-md-12')

postort = []
postnr = []
urls = []
antal = []

for k,x in enumerate(block2):
    abc = block2[k].find_all('a', href=True)
    abcd = block2[k].find_all('span')
    
    for kx, ky in enumerate(abc):      
        tmp = abcd[kx].text
        tmp = tmp.replace('\xa0', '')
        tmp = tmp.replace('(', '')
        tmp = tmp.replace(')', '')
        
        tmp2 = abc[kx].text
        tmp2 = tmp2.split(' - ')
        post_ort = tmp2[0]
        post_nr = tmp2[1]
        
        
        postort.append(post_ort)
        postnr.append(post_nr)
        urls.append(base_url + abc[kx].attrs['href'])
        antal.append(tmp)
    
post_dic = {}
for k,x in enumerate(postort):
    post_dic[k] = {}
    post_dic[k]['url'] = urls[k]
    post_dic[k]['antal'] = antal[k]
    post_dic[k]['post_ort'] = postort[k]
    post_dic[k]['post_nr'] = postnr[k] 

kommun_sel['nested'] = post_dic


### POST NR ###

URLS2 = []
for k,x in enumerate(kommun_sel['nested']):
    URLS2.append(kommun_sel['nested'][k]['url'])



def something(url_req):
    for ki2, xi2 in enumerate(kommun_sel['nested']):
    
    kommun_gator = kommun_sel['nested'][ki2]
    tmp7 = url_req
    gator_top = requests.get(tmp7).text
    
    soup = BeautifulSoup(gator_top, 'html.parser')  
    block = soup.find_all('div', class_='content-block')
    block2 = block[1].find_all('div', class_='link-structure__col2 col-xs-12 col-md-12') # 1
    
    gator = []
    urls = []
    antal = []
    
    for k,x in enumerate(block2):
        abc = block2[k].find_all('a', href=True)
        abcd = block2[k].find_all('span')
        
        for kx, ky in enumerate(abc):      
            tmp = abcd[kx].text
            tmp = tmp.replace('\xa0', '')
            tmp = tmp.replace('(', '')
            tmp = tmp.replace(')', '')
            
            tmp2 = abc[kx].text
    
            
            
            gator.append(tmp2)
            urls.append(base_url + abc[kx].attrs['href'])
            antal.append(tmp)
        
    gator_dic = {}
    for k,x in enumerate(gator):
        gator_dic[k] = {}
        gator_dic[k]['url'] = urls[k]
        gator_dic[k]['antal'] = antal[k]
        gator_dic[k]['gator'] = gator[k]
    
    kommun_gator['nested'] = gator_dic
   
    ### GATOR ###
    for ki3, xi3 in enumerate(gator_dic):
        try_fail = 0
        postnr_addreser = gator_dic[ki3]
        addreser_top = requests.get(postnr_addreser['url']).text
        
        soup = BeautifulSoup(addreser_top, 'html.parser')  
        block = soup.find_all('div', class_='content-block')
        try:
            block2 = block[1].find_all('div', class_='link-structure__col3 col-xs-12 col-md-12') # 2
        except:
            try_fail = 1
            pass
#            
        if try_fail == 1:
            try:
                print("trying this shiet")
                f2 = open('try.log', 'a+')
                print(ki3, postnr_addreser['url'], file=f2)

                block = soup.find('div', class_='x-left col-md-12 col-lg-12 x-left--org-padding x-left--no-x-m-small-screens ')
                block2 = block.find_all('div', class_='search-list-item')
                
                for ky, kx in enumerate(block2):
                    company = {}
                    
                    company_url = str(base_url + block2[ky].find('a', class_='search-list-content').attrs['href'])
                    
                    org_nr = block2[ky].find('a', class_='search-list-content').attrs['href']
                    org_nr = org_nr.split('-')
                    org_nr = org_nr[0]
                    org_nr = str(org_nr[1:])
                    
                    company_name = str(block2[ky].find('span', class_='name').text)
                    company_address = str(block2[ky].find('span', class_='address').text)
                    
                    company_type = str(block2[ky].find('span', class_='iconStartFa2').text)
                    
                    company['company_url'] = company_url
                    company['org_nr'] = org_nr
                    company['company_name'] = company_name
                    company['company_address'] = company_address
                    company['company_type'] = company_type
                    
                    company_database.append(company)
            except:
                
                f = open('error.log', 'a+')
                print(ki3, postnr_addreser['url'], file=f)

                print("not work")
                
                pass

        
        addreser = []
        urls = []
        antal = []
        
        for k,x in enumerate(block2):
            abc = block2[k].find_all('a', href=True)
            abcd = block2[k].find_all('span')
            
            for kx, ky in enumerate(abc):      
                tmp = abcd[kx].text
                tmp = tmp.replace('\xa0', '')
                tmp = tmp.replace('(', '')
                tmp = tmp.replace(')', '')
                
                tmp2 = abc[kx].text
        
                
                
                addreser.append(tmp2)
                urls.append(base_url + abc[kx].attrs['href'])
                antal.append(tmp)
            
        addreser_dic = {}
        for k,x in enumerate(addreser):
            addreser_dic[k] = {}
            addreser_dic[k]['url'] = urls[k]
            addreser_dic[k]['antal'] = antal[k]
            addreser_dic[k]['gator'] = addreser[k]
        
        postnr_addreser['nested'] = addreser_dic
        
        ### FORETAG ###
        for ki4, xi4 in enumerate(addreser_dic):
            foretag_addreser = addreser_dic[ki4]
            foretag = requests.get(foretag_addreser['url']).text
            
            soup = BeautifulSoup(foretag, 'html.parser')  
            block = soup.find('div', class_='x-left col-md-12 col-lg-12 x-left--org-padding x-left--no-x-m-small-screens ')
            block2 = block.find_all('div', class_='search-list-item') # 3
            
            
            for ky, kx in enumerate(block2):
                company = {}
                
                company_url = str(base_url + block2[ky].find('a', class_='search-list-content').attrs['href'])
                
                org_nr = block2[ky].find('a', class_='search-list-content').attrs['href']
                org_nr = org_nr.split('-')
                org_nr = org_nr[0]
                org_nr = str(org_nr[1:])
                
                company_name = str(block2[ky].find('span', class_='name').text)
                company_address = str(block2[ky].find('span', class_='address').text)
                
                company_type = str(block2[ky].find('span', class_='iconStartFa2').text)
                
                company['company_url'] = company_url
                company['org_nr'] = org_nr
                company['company_name'] = company_name
                company['company_address'] = company_address
                company['company_type'] = company_type
                
                company_database.append(company)
#                print(len(company_database))

# with concurrent.futures.ThreadPoolExecutor(max_workers=35) as executor:
#     future_to_url = {executor.submit(something, url5): kx5 for kx5, url5 in enumerate (URLS2)}
#     for future in concurrent.futures.as_completed(future_to_url):
#         url = future_to_url[future]
#         output3[url] = future.result()
#         print(len(output3))

#f.close()

#return company_database

#%%
                    
#with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
#    future_to_url = {executor.submit(foretag_scraper, kx4): kx4 for kx4, url in enumerate (URLS)}
#    for future in concurrent.futures.as_completed(future_to_url):
#        url = future_to_url[future]
#        output2[url] = future.result()
#        print(len(output2))